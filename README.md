<h1>Amplifire Social Network</h1>

Project Description
In this social network, everyone is friends with everyone. As a user, you will be able to register and login to your account. When you successfully login, you are automatically friends with everyone which means you will see a feed of everyone's posts. Users will be able to create a post and like other people's posts. Users will also have the ability to view other profiles which will display posts of that specific user.

<b>Technologies Used</b>

-Angular Version 12

-Spring Version 5.3.10

-Hibernate Version 5.6.0

-PostgreSql Version 42.2.23


<b>Features</b>

-Register accounts.

-Update profiles.

-Post content, including images, description, youtube videos.

-React to posted content.

-Public Chat Room.

<b>To-do list:</b>

-Improve Like Feature Implementation for more efficiency.

-Add mobile first responsive layouts for all views.

<b>Getting Started</b>

git clone https://gitlab.com/eduar2gp/afire_social_network.git

Environment Variables

DB_ENDPOINT = IP Address of Database

DB_NAME = Database name

DB_USER = Database user

DB_PASSWORD = Database password

EMAIL_USERNAME = Email for sending Recovery Password email

EMAIL_PASSWORD = Email Password


<b>License</b>

Free Open source. Feel free to modify and redistribute.

# Contributors
Andrew Carr @Zone50o

Santiago @ratedsant 

